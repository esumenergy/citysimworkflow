Zurich.3dm is the original 3D Rhino model 
Finalized: 13/11/17 
Developed by: Martin Bielik, martin.bielik@uni-weimar.de

-Each layer is assigned by EGID value using Open Street Map building footprints
-Develop building blocks with average height according to confidential building stock database
-Used the number of stories and multiplied by 3.5m 