DB data folder
Building statistics data can be placed here as simulation inputs,
but sensitive data related to the project is currently not open here.

geometry folder
1. These are the raw complete geometries from Martin. 
2. XML file is the saved output from CitySim
3. To keep it simple and clean, it is recommended not to modify things on these files