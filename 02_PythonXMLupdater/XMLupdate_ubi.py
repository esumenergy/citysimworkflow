####Uniform building input - xml parser without building-specific information 

import xml.etree.ElementTree as et

## 01 Access existing XML file with parse
tree = et.parse('input/Baseline.xml')
root = tree.getroot()

## 02 Explore relevant xml elements and attibutes
#Finds all children nodes in the xml file
for child in root:
    print(child.tag, child.attrib)
#Finds all Building elements in the xml file and lists the attributes
for Building in root.iter('Building'):
    print(Building.attrib)
#Finds all Wall elements in Building 
for Wall in Building.iter('Wall'):
    print(Wall.attrib)

## 03 Find and replace various element attributes
for Climate in root.iter('Climate'):
    Climate.set('location', 'Weimar.cli') #add climate file to XML
    
for Building in root.iter('Building'):
    Building.set('Ninf', '0.45') #infiltration rates
   
for Zone in root.iter('Zone'):
    Zone.set('Tmin', '22') #minimum temperature setpoints

for Wall in root.iter('Wall'):
    Wall.set('GlazingRatio','0.4') #constant window to wall ratio (WWR)
    Wall.set('GlazingGValue','0.7') #constant glazing g-value
    Wall.set('GlazingUValue','2') #constant glazing u-value
    Wall.set('OpenableRatio','0.75') #constant operable ratio 

## 04 Write new XML file in 'output' folder, ready for simulation
tree.write('output/Baseline_updatedXML.xml') # Note to always execute 'tree.write' outside of for loop
