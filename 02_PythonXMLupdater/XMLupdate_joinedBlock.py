import xml.etree.ElementTree as ET
import psycopg2
import pandas as pd
import numpy as np
import os
import time

cwd = os.getcwd()  # current working directory

###### Set up DB connection here #####
conn = psycopg2.connect("host=localhost dbname=scenario01 user=postgres password=ia09")
###### Set up input XML file path here ######
tree = ET.parse (cwd + "/input/baseline/completeModel.xml")
xmlRoot = tree.getroot()
###### Set up output XML file path here ######
folderName = 'scenario01'
outputName = '/completeModel_updated.xml'
outputPath = cwd + '/output/' + folderName
if not os.path.exists(outputPath):
    os.makedirs(outputPath)
outputPath += outputName


for Climate in xmlRoot.iter('Climate'):
    Climate.set('location', 'Zurich.cli') #add climate file to XML

def joinedBlockedQuery(BuildingID):
    ###### Execute DB SQL query #####
    cur = conn.cursor()

    # Retrieve data from DB, this table is joined by three tables
    cur.execute(
        """
        select IDmap."BuildingID", statistic.*, bTypes."ClassifiedType", bTypes."cTypeID"
        from public."Statistic" as statistic join public."map" as IDmap on statistic."EGID" = IDmap."EGID"
        join public."buildingTypes" as bTypes on statistic."GdbArt1N" = bTypes."BuildingType"
        where IDmap."BuildingID" = """ + str(BuildingID) + """;
        """
    )
    # table content
    data = cur.fetchall()
    # table schema
    colnames = [desc[0] for desc in cur.description]
    # convert into pd.DataFrame
    retrieved = pd.DataFrame(data, columns=colnames)

    return retrieved



def attributeClass(ID):
    class building:
        def __init__(self, occupants, Ninf, Type):
            self.occupants = str(occupants)
            self.Ninf = str(Ninf)
            self.occupancyProfile = str(Type)

    class walls:
        def __init__(self, GlazingRatio, GlazingGValue, GlazingUValue, OpenableRatio, Type):
            self.GlazingRatio = str(GlazingRatio)
            self.GlazingGValue = str(GlazingGValue)
            self.GlazingUValue = str(GlazingUValue)
            self.OpenableRatio = str(OpenableRatio)
            self.Type = str(Type)

    class roofs:
        def __init__(self, GlazingRatio, GlazingGValue, GlazingUValue, OpenableRatio, Type):
            self.GlazingRatio = str(GlazingRatio)
            self.GlazingGValue = str(GlazingGValue)
            self.GlazingUValue = str(GlazingUValue)
            self.OpenableRatio = str(OpenableRatio)
            self.Type = str(Type)

    # execute SQL query with the given buildingID.
    retrieved = joinedBlockedQuery(ID)

    # applying decision rule or classification to characterize building attributes.
    bVariables = decisionRule(retrieved)

    # specify parameters from constants or calculated variables
    buildingAttrib = building(bVariables.nOccupants, bVariables.Ninf, bVariables.profileID)
    wallsAttrib = walls(0.3, 0.7, 1.3, 0.5, 4)
    roofsAttrib = roofs(0.1, 0, bVariables.uValues, 0, 12)

    return buildingAttrib, wallsAttrib, roofsAttrib


def decisionRule(retrieved):
    class buildingVariables:
        def __init__(self, nOccupants, Ninf, uValues, profileID):
            self.nOccupants = nOccupants
            self.Ninf = Ninf
            self.uValues = uValues
            self.profileID = profileID

    # specify parameters with defined conditions
    nOccupants = sum(retrieved.iloc[:]['PersTotal'])  # sum all people in the aggregated building
    builtYear = np.mean(retrieved.iloc[:]['Baujahr'])  # use average builtYears for the aggregated building


    if builtYear > 2000:
        Ninf = 0.1
        uValues = 0.3
    elif 1900 < builtYear <= 2000:
        Ninf = 0.3
        uValues = 0.25
    else:
        Ninf = 0.4
        uValues = 0.2

    # define aggregated building type based on majority of single building types in the zone
    profileTypeIDs = []
    for i in retrieved.iloc[:]['cTypeID']:
        profileTypeIDs.append(i)
    profileID = max(profileTypeIDs, key=profileTypeIDs.count)

    variables = buildingVariables(nOccupants, Ninf, uValues, profileID)

    return variables



def xmlUpdate(ID, buildingRoot):

    # the function attributeClass() defines building specific properties and output them as variables
    buildingAttrib, wallsAttrib, roofsAttrib = attributeClass(ID)

    # Updating building attributes in XML file
    buildingID = int(buildingRoot.get('key'))

    if buildingID == ID:
        buildingRoot.set('Ninf', buildingAttrib.Ninf)

        for zone in buildingRoot.iter('Zone'):
            # zoneID = zone.get('id')
            for occupancy in zone.iter('Occupants'):
                occupancy.set('n', buildingAttrib.occupants)
                occupancy.set('type', buildingAttrib.occupancyProfile)
            for wall in zone.iter('Wall'):
                wall.set('type', wallsAttrib.Type)
                wall.set('GlazingRatio', wallsAttrib.GlazingRatio)
                wall.set('GlazingGValue', wallsAttrib.GlazingGValue)
                wall.set('GlazingUValue', wallsAttrib.GlazingUValue)
                wall.set('OpenableRatio', wallsAttrib.OpenableRatio)
            for roof in zone.iter('Roof'):
                roof.set('type', roofsAttrib.Type)
                roof.set('GlazingRatio', roofsAttrib.GlazingRatio)
                roof.set('GlazingGValue', roofsAttrib.GlazingGValue)
                roof.set('GlazingUValue', roofsAttrib.GlazingUValue)
                roof.set('OpenableRatio', roofsAttrib.OpenableRatio)
    else:
        print "wrong matched in ID:", ID


def updateAll(xmlRoot):
    startTime = time.time()

    for buildingRoot in xmlRoot.iter('Building'):
        buildingID = int(buildingRoot.get('key'))
        xmlUpdate(buildingID, buildingRoot)
    tree.write(outputPath)

    endTime = time.time()
    elapsed_time = endTime - startTime
    print "executing time = ", elapsed_time

updateAll(xmlRoot)
