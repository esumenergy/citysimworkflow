
-XMLupdate_bsi_EGID - This is the xml parser for the "bsi" or building specific input by EGID
-XMLupdate_bsi_zone - This ist he xml parser for the "bsi_zone" or building specific input by Zone (1 or more EGID per building block)
-XMLupdate_ubi - This is the xml parser for the "ubi" or the uniform building input

-"input" folder - contains the original xml file for each scenario 
-"output" folder - contains the updated xml file/folder for each scenario 

-The climate file .cli must be in the "output/project" folder assuming this is where the citysim.exe runs the file of interest
-The output data for 4 select days are included in this repository as examples of extreme days days where: 

-Note the naming convention for each of the four select simulations
S01-SpringExuinox
S02-SummerSolstice
S03-FallEquinox
S04-WinterSolstice 

