# -*- coding: utf-8 -*-
import xml.etree.ElementTree as ET
import psycopg2
import pandas as pd
import numpy as np
import sys

reload(sys)
sys.setdefaultencoding('utf8')


buildingTypes = pd.read_csv('classificationInfo/buildingType.csv')
for i in buildingTypes.iloc[:]['BuildingType'].astype(str):
    print i

def joinedBlockedQuery(BuildingID):
    ###### Execute DB SQL query #####
    conn = psycopg2.connect("host=localhost dbname=CitySimData user=postgres password=ia09")
    cur = conn.cursor()

    # Retrieve data from DB
    cur.execute(
        """
        select IDmap."BuildingID", statistic.* 
        from public."Statistic" as statistic join public."map" as IDmap on statistic."EGID" = IDmap."EGID"
        where IDmap."BuildingID" = """ + str(BuildingID) + """;
        """
    )
    # table content
    data = cur.fetchall()
    # table schema
    colnames = [desc[0] for desc in cur.description]
    # convert into pd.DataFrame
    retrieved = pd.DataFrame(data, columns=colnames)
    return retrieved


def buildingType(ID):
    conn = psycopg2.connect("host=localhost dbname=CitySimData user=postgres password=ia09")
    cur = conn.cursor()

    cur.execute(
        """
        SELECT
         DISTINCT "GdbArt1N"
        FROM
          public."Statistic";
            """
    )
    data = cur.fetchall()
    colnames = [desc[0] for desc in cur.description]
    retrieved = pd.DataFrame(data, columns=colnames)
    return retrieved

retrieved = joinedBlockedQuery(1242)
buildingType = buildingType(1242)

# typeCounts = []
# for i in retrieved.iloc[:]['GdbArt1N']:
#     typeId = 1
#     for j in buildingType.iloc[:]['GdbArt1N']:
#         typeId += 1
#         if i == j:
#             typeCounts.append(typeId)
#             print i,j, typeId
#
# # for j in buildingTypes.iloc[:]['BuildingType']:
# #     print j

typeCounts = []
# define aggregated building type based on majority of single building type in the zone
for i in retrieved.iloc[:]['GdbArt1N']:
    typeCounts.append(i)
buildingType = max(typeCounts, key=typeCounts.count)
# print buildingType

print "Spital- und Klinikgebäude"
print unicode('Spital- und Klinikgebäude',  errors='ignore')
print u'Zürich'