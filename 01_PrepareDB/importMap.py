##############
# Given Map file, this script automatically creates table and imports data into the
# database. Be noted that the defined database schema here is only valid for this project.
# If encounter any encoding problem, open the data with notepad, change the encoding to "UTF-8" and save as csv.
# Author: Cheng-Kai Wang, Email: ckwang25@gmail.com
# Date: 15.02.2018
##############

import psycopg2
import os
import csv

cwd = os.getcwd()  # current working directory

def importData(filePath, conn):
    cur = conn.cursor()
    cur.execute(
        """
        -- Table: public.map

        DROP TABLE IF EXISTS public."map";
        
        CREATE TABLE public."map"
        (
            "EGID" integer,
            "BuildingID" integer NULL,
            "Building_X" double precision,
            "Building_Y" double precision,
            "Xkoord" double precision,
            "Ykoord" double precision
        )
        WITH (
            OIDS = FALSE
        )
        TABLESPACE pg_default;
        
        ALTER TABLE public."map"
            OWNER to postgres;

        """
    )

    # due to permission issue, using "INSERT" query instead of commonly used "COPY"
    with open(filePath, 'r') as Data:
        reader = csv.reader(Data, delimiter=";")
        next(reader)  # skip first row
        for row in reader:
            emptyIndices = [i for i, x in enumerate(row) if x == '']
            for index in emptyIndices:
                row[index] = None

            cur.execute(
                """INSERT INTO public."map"
                   VALUES (%s, %s, %s, %s, %s, %s)""",
                row
            )

    cur.close()
    conn.commit()

# If run individual script with individual connection and file path setting, specify inside the function.
# Otherwise, it will run with the given path and connection setting specified in importAll()
def main(filePath_map = None, connPsql = None):
    if filePath_map and connPsql is not None:
        filePath = filePath_map
        conn = connPsql
    else:
        # default file path and database connection setting
        filePath = cwd + '/../00_Geometry_DBdata/DBdata/171113_EGID to BuildingID_building blocks average height.csv'
        conn = psycopg2.connect("host=localhost dbname=baseline user=postgres password=ia09")

    importData(filePath, conn)



if __name__ == "__main__":
    main()

