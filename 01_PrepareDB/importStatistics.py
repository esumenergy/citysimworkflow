##############
# Given building properties (statistics) data, this script automatically creates table and imports data into the
# database. Be noted that the defined database schema here is only valid for this project.
# If encounter any encoding problem, open the data with notepad, change the encoding to "UTF-8" and save as csv.
# Author: Cheng-Kai Wang, Email: ckwang25@gmail.com
# Date: 15.02.2018
##############

import psycopg2
import os
import csv

cwd = os.getcwd()  # current working directory

def importData(filePath, conn):
    cur = conn.cursor()
    cur.execute(
        """
        -- Table: public."Statistic"

        DROP TABLE IF EXISTS public."Statistic";

        CREATE TABLE public."Statistic"
        (
            "EGID" integer NOT NULL,
            "Adresse" character varying COLLATE pg_catalog."default",
            "Xkoord" double precision,
            "Ykoord" double precision,
            "QuartierC" smallint,
            "QuartierN" character(50) COLLATE pg_catalog."default",
            "GbdArt1C" double precision,
            "GdbArt1N" character(60) COLLATE pg_catalog."default",
            "Baujahr" smallint,
            "Umbaujahr" smallint,
            "Grundflache_EG" double precision,
            "Dachform_Name" character(60) COLLATE pg_catalog."default",
            "Dachform_Code" character(5) COLLATE pg_catalog."default",
            "Geschosse_UG" smallint,
            "Geochosse_EG_OG" smallint,
            "PersTotal" smallint,
            "Pers_00_09" smallint,
            "Pers_10_19" smallint,
            "Pers_20_29" smallint,
            "Pers_30_39" smallint,
            "Pers_40_49" smallint,
            "Pers_50_59" smallint,
            "Pers_60_69" smallint,
            "Pers_70_79" smallint,
            "Pers_80_89" smallint,
            "Pers_90_99" smallint,
            "Pers_100plus" smallint,
            "EnergieC" double precision,
            "EnergieN" character(40) COLLATE pg_catalog."default",
            "Nutzflache" double precision,
            "entr_neu" double precision,
            CONSTRAINT "Statistic_pkey" PRIMARY KEY ("EGID")
        )
        WITH (
            OIDS = FALSE
        )
        TABLESPACE pg_default;

        ALTER TABLE public."Statistic"
            OWNER to postgres;
        """
    )

    # due to permission issue, using "INSERT" query instead of commonly used "COPY"
    with open(filePath, 'r') as Data:
        reader = csv.reader(Data, delimiter=",")
        next(reader)  # skip first row
        for row in reader:
            emptyIndices = [i for i, x in enumerate(row) if x == '']
            for index in emptyIndices:
                row[index] = None

            cur.execute(
                """INSERT INTO public."Statistic"
                   VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s,
                   %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)""",
                row
            )

    # if needed, change the data type of the columns here
    # cur.execute(
    #     """
    #     ALTER TABLE public."Statistic"
    #     ALTER COLUMN "QuartierC" TYPE int USING "QuartierC"::integer;
    #     """
    # )

    cur.close()
    conn.commit()


# If run individual script with individual connection and file path setting, specify inside the function.
# Otherwise, it will run with the given path and connection setting specified in importAll()
def main(filePath_Statistic = None, connPsql = None):
    if filePath_Statistic and connPsql is not None:
        filePath = filePath_Statistic
        conn = connPsql
    else:
        # default file path and database connection setting
        filePath = cwd + '/../00_Geometry_DBdata/DBdata/Statistic.csv'
        conn = psycopg2.connect("host=localhost dbname=baseline user=postgres password=ia09")

    importData(filePath, conn)


if __name__ == "__main__":
    main()
