import numpy as np
import pandas as pd
import psycopg2
import os
import csv


##############
# This script is simply used for testing some new functions
##############

def query():
    ###### Execute DB SQL query #####
    conn = psycopg2.connect("host=localhost dbname=CitySimData user=postgres password=ia09")
    cur = conn.cursor()

    # Retrieve data from DB, this table is joined by three tables
    cur.execute(
        """
        select * from public."Area";
        """
    )
    # table content
    data = cur.fetchall()
    # table schema
    colnames = [desc[0] for desc in cur.description]
    # convert into pd.DataFrame
    retrieved = pd.DataFrame(data, columns=colnames)

    print retrieved
    return retrieved

def updateNewData():
    conn = psycopg2.connect("host=localhost dbname=CitySimData user=postgres password=ia09")
    cur = conn.cursor()

    # Retrieve data from DB, this table is joined by three tables
    cur.execute(
        """
    DROP TABLE public."Area";
        
    CREATE TABLE public."Area"
    (
        "BuildingID" integer,
        "Floor_m2" double precision,
        "Roof_m2" double precision,
        "Wall_m2" double precision,
        "Windows_m2" double precision
    )
    WITH (
        OIDS = FALSE
    )
    TABLESPACE pg_default;
    
    ALTER TABLE public."Area"
        OWNER to postgres;
        """
    )

    with open('C:\Users\Danielle\Desktop\CompleteWorkflow\cleaned_Area.csv', 'r') as Output:
        # Notice that we don't need the `csv` module.
        reader = csv.reader(Output)
        next(reader)
        for row in reader:
            cur.execute(
                """INSERT INTO public."Area" VALUES (%s, %s, %s, %s, %s)""",
                row
            )

    cur.close()
    conn.commit()

updateNewData()
query()